FROM java:8-alpine
MAINTAINER Your Name <you@example.com>

ADD target/uberjar/moo.jar /moo/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/moo/app.jar"]
