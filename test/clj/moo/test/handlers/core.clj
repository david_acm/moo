(ns moo.test.handlers.core
  (:require [moo.handlers.core :refer :all]
            [clojure.test :refer :all]))

(deftest delete-track
  (testing "Deleting a file track"
    (let [room-id "room-id"
          track-id "track-id"
          actions (-event-msg-handler {:id :track/delete
                                       :?data track-id
                                       :room-id room-id
                                       :database-fns {:get-room-state-fn
                                                      (fn [id]
                                                        {:tracks [{:id track-id
                                                                   :track-type "file"
                                                                   :src "/resources/uploads/test1.mp3"}]})}})]

      (is (= [[:delete-track [room-id track-id]]
              [:broadcast-to-room [room-id [:track/delete track-id]]]
              [:files-to-delete ["/uploads/test1.mp3"]]]
             actions))))

  (testing "Deleting a Youtube track"
    (let [room-id "room-id"
          track-id "track-id"
          actions (-event-msg-handler {:id :track/delete
                                       :?data track-id
                                       :room-id room-id
                                       :database-fns {:get-room-state-fn
                                                      (fn [id]
                                                        {:tracks [{:id track-id
                                                                   :track-type :youtube
                                                                   :src "https://www.youtube.com/watch?v=N-Dyaoq_oSA"}]})}})]

      (is (= [[:delete-track [room-id track-id]]
              [:broadcast-to-room [room-id [:track/delete track-id]]]]
             actions))))

  (testing "Deleting a track that doesn't exist"
    (let [room-id "room-id"
          track-id "track-id"
          uid "uid"
          actions (-event-msg-handler {:id :track/delete
                                       :?data track-id
                                       :room-id room-id
                                       :uid uid
                                       :database-fns {:get-room-state-fn
                                                      (fn [id]
                                                        {:tracks []})}})
          action (first actions)]

      (is (= (first action) :send-to-client))
      (let [data (second action)]
        (is (= (first data) uid))
        (is (= (first (second data)) :error))
        (is (string? (second (second data))))))))

(deftest new-external-track
  (testing
   (let [room-id "room-id"
         user-id "user-id"
         track-id "track-id"
         uploads-dir "/uploads"
         test-track {:user-id user-id :id track-id}
         url "https://test.com/test.mp3"
         file-extension ".mp3"
         actions (-event-msg-handler {:id :track/new-external
                                      :room-id room-id
                                      :user-id user-id
                                      :?data url
                                      :uploads-dir uploads-dir
                                      :unique-id-fn (fn [] track-id)})
         add-track-action (nth actions 0)
         broadcast-to-room-action (nth actions 1)
         download-track-action (nth actions 2)]
     (is (= add-track-action [:add-track [room-id test-track]]))
     (is (= broadcast-to-room-action [:broadcast-to-room [room-id [:track/new test-track]]]))
     (is (= download-track-action [:download-track {:url url
                                                    :track-id track-id
                                                    :room-id room-id
                                                    :dest (str uploads-dir "/" track-id file-extension)}])))))
