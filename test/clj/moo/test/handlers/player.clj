(ns moo.test.handlers.player
  (:require [moo.handlers.core :refer :all]
            [moo.handlers.player]
            [clojure.test :refer :all]))

(deftest resync
  (testing "Should return player state and track order"
    (let [client-id "test-client-id"
          client {:id client-id :synced? false}
          room-id "test-room-id"
          player {:room-id room-id}
          tracks [{:id 1} {:id 2}]
          track-order [1 2]
          actions (-event-msg-handler {:id :player/resync
                                       :client client
                                       :room-id room-id
                                       :database-fns {:get-room-state-fn
                                                      (fn [id]
                                                        {:player {:room-id id}
                                                         :tracks tracks})}})
          [update-client-id update-client-data] (first actions)
          [action-id action-data] (second actions)
          [event-id event-data] (second action-data)]
      (is (= :update-client update-client-id))
      (is (= [room-id {:id client-id :synced? true}] update-client-data))

      (is (= :send-to-client action-id))
      (is (= (:id client) (first action-data)))
      (is (= :player/resync event-id))
      (is (= {:player player :track-order track-order} event-data)))))

(deftest desync
  (testing "Should return player state"
    (let [client-id "test-client-id"
          client {:id client-id :synced? true}
          room-id "test-room-id"
          actions (-event-msg-handler {:id :player/desync
                                       :room-id room-id
                                       :client client})
          [update-client-id update-client-data] (first actions)
          [action-id action-data] (second actions)
          [event-id event-data] (second action-data)]
      (is (= :update-client update-client-id))
      (is (= [room-id {:id client-id :synced? false}] update-client-data)))))

(deftest set-track
  (testing "Should send to client and send action"
    (let [room-id "test-room"
          track-id "test-track"
          player-data {:track-id track-id
                       :player-pos {:last-stop-pos 0}}
          timestamp 1
          actions (-event-msg-handler {:id :player/set-track
                                       :room-id room-id
                                       :?data player-data
                                       :timestamp timestamp})]
      (is (= [:set-track [room-id player-data]] (first actions)))
      (is (= [:broadcast-to-room [room-id [:player/set-track player-data]]] (second actions))))))

(deftest player-ended
  (testing "Should continue to next track if no other synced clients and autoplay is true"
    (let [room-id "test-room"
          client-id "test-client"
          client {:id client-id :synced? true}
          room-state {:tracks [{:id 1} {:id 2}]
                      :player {:track-id 1 :autoplay? true}}
          event-msg {:id :player/ended
                     :room-id room-id
                     :client {:id client-id}
                     :clients-in-room {client-id client}
                     :database-fns {:get-room-state-fn
                                    (fn [id]
                                      (get {room-id room-state} id))}}
          actions (-event-msg-handler event-msg)]
      (is (= (-event-msg-handler (assoc event-msg
                                        :id :player/set-track
                                        :?data {:track-id 2
                                                :player-pos {:last-stop-point 0}
                                                :paused? nil}))
             actions))))

  (testing "Should update client has ended and do nothing if not everyone has ended"
    (let [room-id "test-room"
          client-id "test-client"
          client-id2 "test-client2"
          client {:id client-id :synced? true}
          client2 {:id client-id2 :synced? true}
          actions (-event-msg-handler {:id :player/ended
                                       :room-id room-id
                                       :client {:id client-id}
                                       :clients-in-room {client-id client
                                                         client-id2 client2}})]
      (is (= [:update-client [room-id {:id client-id :ended? true}]] (nth actions 0)))
      (is (= 1 (count actions)))))

  (testing "Should change track if all synced users have ended and autoplay is on"
    (let [room-id "test-room"
          client-id "test-client"
          client-id2 "test-client2"
          client {:id client-id :synced? true}
          client2 {:id client-id2 :ended? true :synced? true}
          room-state {:tracks [{:id 1} {:id 2}]
                      :player {:track-id 1 :autoplay? true}}
          event-msg {:id :player/ended
                     :room-id room-id
                     :client client
                     :clients-in-room {client-id client
                                       client-id2 client2}
                     :database-fns {:get-room-state-fn
                                    (fn [id]
                                      (get {room-id room-state} id))}}
          actions (-event-msg-handler event-msg)]
      (is (= (-event-msg-handler (assoc event-msg
                                        :id :player/set-track
                                        :?data {:track-id 2
                                                :player-pos {:last-stop-point 0}
                                                :paused? nil}))
             actions))))

  (testing "Should change track if all synced users have ended and autoplay is on"
    (let [room-id "test-room"
          client-id "test-client"
          client-id2 "test-client2"
          client-id3 "test-client3"
          client {:id client-id :synced? true}
          client2 {:id client-id2 :ended? true :synced? true}
          client3 {:id client-id3 :synced? false}
          room-state {:tracks [{:id 1} {:id 2}]
                      :player {:track-id 1 :autoplay? true}}
          event-msg {:id :player/ended
                     :room-id room-id
                     :client client
                     :clients-in-room {client-id client
                                       client-id2 client2
                                       client-id3 client3}
                     :database-fns {:get-room-state-fn
                                    (fn [id]
                                      (get {room-id room-state} id))}}
          actions (-event-msg-handler event-msg)]
      (is (= (-event-msg-handler (assoc event-msg
                                        :id :player/set-track
                                        :?data {:track-id 2
                                                :player-pos {:last-stop-point 0}
                                                :paused? nil}))
             actions))))

  (testing "Should not change track if autoplay is off"
    (let [room-id "test-room"
          client-id "test-client"
          client-id2 "test-client2"
          client-id3 "test-client3"
          client {:id client-id :synced? true}
          client2 {:id client-id2 :ended? true :synced? true}
          client3 {:id client-id3 :synced? false}
          room-state {:tracks [{:id 1} {:id 2}]
                      :player {:track-id 1 :autoplay? false}}
          event-msg {:id :player/ended
                     :room-id room-id
                     :client client
                     :clients-in-room {client-id client
                                       client-id2 client2
                                       client-id3 client3}
                     :database-fns {:get-room-state-fn
                                    (fn [id]
                                      (get {room-id room-state} id))}}
          actions (-event-msg-handler event-msg)]
      (is (= [:update-client [room-id {:id client-id :synced? true :ended? true}]] (nth actions 0)))
      (is (= 1 (count actions))))))
