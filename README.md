# moo

Share media with friends

![](http://rateless.net/~dormo/img/moo0.png)

## Prerequisites

Currently, only Java 8 is supported.

You will need [Leiningen][1] 2.0 or above installed.

[1]: https://github.com/technomancy/leiningen

## Running

To start a web server for the application, run:

    lein run 

## License

Copyright © 2017 Vincent Heuken

Licensed under the AGPLv3. See LICENSE.txt for more info.
