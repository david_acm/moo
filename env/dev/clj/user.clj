(ns user
  (:require clojure.tools.namespace.repl
            [mount.core :as mount]
            [moo.figwheel :refer [start-fw stop-fw cljs]]
            [moo.db.core]
            [monger.db]
            [moo.cache-manager.core]
            moo.core))

(clojure.tools.namespace.repl/set-refresh-dirs "clj" "cljc")

(defn start []
  (mount/start-without #'moo.core/repl-server)
  (moo.core/resume-downloads!))

(defn stop []
  (mount/stop-except #'moo.core/repl-server))

(defn restart []
  (stop)
  (start))

(defn clear-database []
  (monger.db/drop-db moo.db.core/db))

(defn clear-caches []
  (doseq [[_ c] @moo.cache-manager.core/caches] (.clear c)))
