(ns ^:figwheel-no-load moo.app
  (:require [moo.core :as core]
            [devtools.core :as devtools]))

(enable-console-print!)

(devtools/install!)

(core/init!)
