(ns moo.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[moo started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[moo has shut down successfully]=-"))
   :middleware identity})
