(ns moo.actions
  (:require [clojure.tools.logging :as log]
            [moo.ws :as ws]))

(defmulti -execute-server-action! first)

(defn execute-server-actions! [actions]
  "Dispatches a coll of actions.
   When provided a map, actions will be dispatched based on keys"
  (doseq [action (seq actions)]
    (when (second action) (-execute-server-action! action))))

(defmethod -execute-server-action! :default [[id _]]
  (log/warn "No -execute-servet-action! implementation provided for:" id))

(defmethod -execute-server-action! :send-to-client [[_ [uid data]]]
  (ws/send! uid data))
