(ns moo.cache-manager.actions
  (:require [moo.actions :as actions]
            [moo.cache-manager.core :as cache]))

(defmethod actions/-execute-server-action! :cache/put [[_ [id k v opts]]]
  (cache/put-or-create! id k v opts))

(defmethod actions/-execute-server-action! :cache/remove [[_ [id k]]]
  (cache/remove! id k))
