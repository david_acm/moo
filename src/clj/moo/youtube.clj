(ns moo.youtube
  (:require [clj-http.client :as http]
            [cheshire.core :as json]
            [clojure.set :refer [rename-keys]]))

(def youtube-url-regex #"http(?:s?):\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\.be\/)([\w\-\_]*)(&(amp;)?‌​[\w\?‌​=]*)?")

(defn get-video-id [url]
  (second (re-find youtube-url-regex url)))

(defn get-api-url [video-id api-key]
  (str "/youtube/v3/videos?id=" video-id
       "&part=snippet,statistics,status,contentDetails"
       "&fields=items(id,snippet,statistics,status,contentDetails)"
       "&key=" api-key))

(def api-base "https://www.googleapis.com")

(defn get-metadata! [video-id api-key]
  (-> (http/get (str api-base (get-api-url video-id api-key)))
      :body
      (json/parse-string true)
      (get-in [:items 0 :snippet])
      (select-keys [:channelTitle :title])
      (rename-keys {:channelTitle :artist})))
