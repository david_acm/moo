(ns moo.track.actions
  (:require [moo.actions :as actions]
            [moo.db.core :as db]
            [moo.youtube :as youtube]
            [moo.file :as file]
            [moo.error :as error]
            [clojure.tools.logging :as log]
            [clojure.java.io :as io]
            [mount.core :as mount]
            [clojure.java.io :as io]
            [clojure.string :as string]
            [clj-time.core :as time]
            [clj-time.format :as time-format]
            [clj-time.coerce :as time-coerce]
            [clj-http.client :as http]
            [moo.config :refer [env]]
            [clojure.set :refer [rename-keys]]))

(mount/defstate downloads :start (atom {})
  :stop (doseq [{:keys [thread]} (vals @downloads)]
          (future-cancel thread)))

(defmethod actions/-execute-server-action! :delete-static-files [[_ files]]
  (doseq [file files]
    (io/delete-file (str (System/getProperty "user.dir") "/static/" file))))

(defmethod actions/-execute-server-action! :delete-track [[_ [room-id {:keys [id track-type]}]]]
  (when-let [{:keys [file thread stream]} (@downloads id)]
    (future-cancel thread)
    (swap! downloads dissoc id)
    (io/delete-file file))
  (db/delete-track room-id id))

(defn last-modified-rfc7232 [file]
  ; We have to do this hacky string/replace shit because of a limitation with JodaTime
  ; http://joda-time.sourceforge.net/apidocs/org/joda/time/format/DateTimeFormat.html
  ; We should switch to Java 8 Date-Time api
  (clojure.string/replace
   (time-format/unparse (time-format/formatters :rfc822)
                        (time/from-time-zone (time-coerce/from-long (.lastModified file))
                                             (time/time-zone-for-id "GMT")))
   #"\+0000"
   "GMT"))

(defn start-or-resume-download-request! [url destination-file]
  (http/get url {:as :stream
                 :headers {"Range" (str "bytes=" (.length destination-file) "-")}}))

(defn start-or-resume-download [url destination-file update-fn on-complete-fn]
  "Starts or resumes a download."
  (let [response (start-or-resume-download-request! url destination-file)
        buffer-size (* 1024 10) ; 10 kb
        content-length (Integer. (get-in response [:headers "content-length"] 0))]
    (when (= 200 (:status response))
      (io/delete-file destination-file))
    (with-open [input (:body response)
                output (java.io.FileOutputStream. destination-file true)]
      (let [buffer (make-array Byte/TYPE buffer-size)]
        (loop [size (.read input buffer)
               bytes-read 0
               last-time (System/currentTimeMillis)]
          (when (and (not (Thread/interrupted))
                     (pos? size))
            (.write output buffer 0 size)
            (recur (.read input buffer)
                   (+ bytes-read size)
                   (update-fn bytes-read content-length last-time)))))))
  (when-not (Thread/interrupted)
    (on-complete-fn destination-file)))

(defmethod actions/-execute-server-action! :update-track [[_ [room-id track]]]
  (db/update-track room-id track))

(defmulti handle-new-track! (fn [room-id track] (:type track)))

(defmethod handle-new-track! :youtube [room-id track]
  (future
    (try
      (let [track (assoc track :metadata (youtube/get-metadata! (youtube/get-video-id (:src track))
                                                                (:youtube-api-key env)))]
        (db/update-track room-id track)
        (actions/execute-server-actions! [[:room/broadcast [room-id [:track/update track]]]]))
      (catch Exception e
        (error/report-error e track)))))

(defn update-file-download! [room-id track-id]
  (fn [bytes-read content-length last-time]
    (let [current-time (System/currentTimeMillis)]
      (if (< 333 (- current-time last-time))
        (do
          (actions/-execute-server-action! [:room/broadcast [room-id [:track/update {:id track-id
                                                                                     :track-type :download
                                                                                     :bytes-read bytes-read
                                                                                     :content-length content-length}]]])
          current-time)
        last-time))))

(defn file-download-complete! [room-id track-id]
  (fn [file]
    (locking room-id
      (swap! downloads dissoc track-id)
      (let [track {:id track-id
                   :track-type :file
                   :metadata (file/get-metadata! file)
                   :src (str "/static/" (last (string/split (.getPath file) #"/")))}]
        (actions/execute-server-actions! [[:update-track [room-id track]]
                                          [:room/broadcast [room-id [:track/update track]]]])))))


(defmethod handle-new-track! :file-upload [room-id track]
  (actions/-execute-server-action! [:send-to-client [(:client-id track)
                                                     [:upload/start (-> track
                                                                        (select-keys [:client-upload-id :id])
                                                                        (rename-keys {:id :track-id}))]]]))
(defmethod handle-new-track! :file-download [room-id track]
  (let [source-url (:original-url track)
        file-extension (last (string/split source-url #"\."))
        file-path (str (str (System/getProperty "user.dir") "/static/") (:id track) "." file-extension)
        destination-file (io/file file-path)]
    (swap! downloads
           assoc
           (:id track)
           {:thread (future
                      (try
                        (start-or-resume-download source-url
                                                  destination-file
                                                  (update-file-download! room-id (:id track))
                                                  (file-download-complete! room-id (:id track)))
                        (catch Exception e
                          (error/report-error e track))))
            :file destination-file})))

(defmethod actions/-execute-server-action! :add-track [[_ [room-id track]]]
  (let [track-without-client-data (dissoc track :client-id :client-upload-id)]
    (db/add-track-to-room room-id track-without-client-data)
    (actions/execute-server-actions! [[:room/broadcast [room-id [:track/new track-without-client-data]]]]))
  (handle-new-track! room-id track))

(defmethod actions/-execute-server-action! :set-track [[_ [room-id {:keys [track-id player-pos]}]]]
  (db/set-track room-id track-id player-pos))
