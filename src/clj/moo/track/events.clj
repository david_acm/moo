(ns moo.track.events
  (:require [moo.handlers.core :as handler]
            [moo.youtube :as youtube]
            [clojure.string :as string]))

(defmethod handler/-event-msg-handler :track/new-youtube [{url :?data
                                                           unique-id-fn :unique-id-fn
                                                           user :user
                                                           room-id :room-id
                                                           {youtube-api-key :youtube-api-key} :config}]
  (when-let [video-id (youtube/get-video-id url)]
    {:add-track [room-id
                 {:id (unique-id-fn)
                  :user-id (:id user)
                  :type :youtube
                  :src url
                  :room-id room-id}]}))

(defmethod handler/-event-msg-handler :track/new-file-download [{:keys [room-id user unique-id-fn ?data]}]
  {:add-track [room-id
               {:id (unique-id-fn)
                :original-url ?data
                :type :file-download
                :user-id (:id user)}]})

(defmethod handler/-event-msg-handler :track/new-file-upload [{:keys [room-id user unique-id-fn ?data client]}]
  {:add-track [room-id
               {:id (unique-id-fn)
                :client-upload-id ?data
                :client-id (:id client)
                :type :file-upload
                :user-id (:id user)}]})


(defmethod handler/-event-msg-handler :track/remove [{downloads-dir :downloads-dir
                                                      track-id :?data
                                                      {get-room-state :get-room-state-fn} :database-fns
                                                      :as event-msg}]
  (let [room-state (get-room-state (:room-id event-msg))
        track (first (filter #(= track-id (:id %)) (:tracks room-state)))]
    (if track
      (filterv (complement nil?)
               [[:delete-track [(:room-id event-msg) track]]
                [:room/broadcast [(:room-id event-msg) [:track/delete track-id]]]
                (when (= "file" (:track-type track))
                  [:delete-static-files [(last (string/split (:src track) #"/"))]])])
      [[:send-to-client [(:uid event-msg) [:server/error "Attempted to remove a track that does not exist"]]]])))
