(ns moo.room.events
  (:require [moo.actions :as actions]
            [moo.db.core :as db]
            [moo.ws :as ws]
            [moo.cache-manager.core :as cache]))

(defmethod actions/-execute-server-action! :room/remove-user [[_ [room-id user-id]]]
  (db/remove-user-from-room room-id user-id)
  (ws/broadcast! (keys (cache/fetch :room-clients room-id)) [:user/leave user-id]))

(defmethod actions/-execute-server-action! :room/delete [[_ room-id]]
  (db/delete-room room-id)
  (cache/remove! :room-clients room-id))

(defmethod actions/-execute-server-action! :room/broadcast [[_ [room-id data]]]
  (ws/broadcast! (keys (cache/fetch :room-clients room-id)) data))
