(ns moo.ws
  (:require [taoensso.sente :as sente]
            [taoensso.sente.server-adapters.immutant :refer (get-sch-adapter)]
            [moo.utils :as utils]))

(defn start-socket-server! [event-msg-handler]
  (let [{:keys [ch-recv send-fn connected-uids
                ajax-post-fn ajax-get-or-ws-handshake-fn]}
        ; TODO: Why do we supply `:user-id-fn`? Isn't this handled in handler.clj?
        (sente/make-channel-socket! (get-sch-adapter) {:user-id-fn (fn [req] (utils/uuid))})]
    (def send! send-fn)
    (def ring-ajax-get-or-ws-handshake ajax-get-or-ws-handshake-fn)
    {:ring-ajax-get-or-ws-handshake ajax-get-or-ws-handshake-fn
     :connected-uids connected-uids
     :ch-recv ch-recv
     :send-fn send-fn
     :stop-fn (sente/start-chsk-router! ch-recv event-msg-handler)}))

(defn broadcast! [uids event]
  (doseq [uid uids]
    (send! uid event)))
