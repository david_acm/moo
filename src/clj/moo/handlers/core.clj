(ns moo.handlers.core
  (:require [moo.ws :as ws]
            [moo.utils :as utils]
            [moo.db.core :as db]
            [clojure.tools.logging :as log]
            [mount.core :as mount]
            [pantomime.extract :as extract]
            [moo.youtube :as youtube]))

(defmulti -event-msg-handler :id)

(defmethod -event-msg-handler :default [event-msg]
  (log/error "No -event-msg-handler implementation provided for:" (:id event-msg) event-msg))

(defmethod -event-msg-handler :chsk/ws-ping [_]
  (log/info "PONG"))

(defmethod -event-msg-handler :track/set-index [event-msg]
  (let [get-room-state (-> event-msg :database-fns :get-room-state-fn)
        room-id (:room-id event-msg)
        tracks (:tracks (get-room-state room-id))
        id (:id (:?data event-msg))
        index (:index (:?data event-msg))]
    (when (and (>= index 0)
               (< index (count tracks)))
      (let [track (nth tracks (.indexOf (map :id tracks) id))]
        [[:set-track-at-index [room-id [track index]]]
         [:room/broadcast [room-id [:track/set-index {:id id :index index}]]]]))))

(defmethod -event-msg-handler :player/play [{room-id :room-id}]
  (db/unpause room-id)
  {:room/broadcast [room-id [:player/play]]})

(defmethod -event-msg-handler :player/pause [{stop-pos :?data
                                              room-id :room-id}]
  (db/pause room-id stop-pos)
  {:room/broadcast [room-id [:player/pause stop-pos]]})

(defmethod -event-msg-handler :player/enable-autoplay [event-msg]
  (db/enable-autoplay (:room-id event-msg))
  {:room/broadcast [(:room-id event-msg) [:player/enable-autoplay]]})

(defmethod -event-msg-handler :player/disable-autoplay [event-msg]
  (db/disable-autoplay (:room-id event-msg))
  {:room/broadcast [(:room-id event-msg) [:player/disable-autoplay]]})

(defmethod -event-msg-handler :player/seek [{:keys [room-id ?data]}]
  (let [player-pos {:last-start-timestamp (quot (System/currentTimeMillis) 1000)
                    :last-stop-point ?data}]
    (->
     room-id
     (db/get-room-by-name)
     (assoc-in [:player :player-pos] player-pos)
     (db/set-room-state))
    {:room/broadcast [room-id [:player/seek player-pos]]}))
