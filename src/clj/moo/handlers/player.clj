(ns moo.handlers.player
  (:require [moo.handlers.core :as handler]
            [moo.shared.utils :as shared-utils]
            [clojure.algo.generic.functor :refer (fmap)]))

(defn all-synced-clients-ended? [clients-map]
  (= 0 (count (filter #(and (not (:ended? %)) (:synced? %)) (vals clients-map)))))

(defmethod handler/-event-msg-handler :player/resync [{client :client
                                                       room-id :room-id
                                                       {get-room-state :get-room-state-fn} :database-fns}]
  [[:cache/put [:room-clients room-id (:id client) (assoc client :synced? true)]]
   (let [room-state (get-room-state room-id)]
     [:send-to-client [(:id client) [:player/resync {:player (:player room-state)
                                                     :track-order (map :id (:tracks room-state))}]]])])

(defmethod handler/-event-msg-handler :player/desync [{:keys [room-id client]}]
  [[:cache/put [:room-clients room-id (:id client) (assoc client :synced? false)]]])

(defmethod handler/-event-msg-handler :player/set-track [{room-id :room-id
                                                          clients-in-room :clients-in-room
                                                          player-data :?data}]
  [[:set-track [room-id player-data]]
   [:cache/put [:room-clients
                room-id
                (fmap #(dissoc % :ended?)
                     clients-in-room)]]
   [:room/broadcast [room-id [:player/set-track player-data]]]])

(defmethod handler/-event-msg-handler :player/ended [{client :client
                                                      room-id :room-id
                                                      clients-in-room :clients-in-room
                                                      {get-room-state :get-room-state-fn} :database-fns
                                                      :as event-msg}]
  (let [update-client-action [:cache/put [:room-clients room-id (assoc clients-in-room (:id client) (assoc client :ended? true))]]]
    (if (or (= 1 (count clients-in-room))
            (all-synced-clients-ended? (dissoc clients-in-room (:id client))))
      (let [room-state (get-room-state room-id)]
        (if (-> room-state :player :autoplay?)
          (let [current-track-id (-> room-state :player :track-id)
                next-track-id (:id (shared-utils/get-next-track (:tracks room-state) current-track-id))]
            (handler/-event-msg-handler (assoc event-msg :id :player/set-track
                                               :?data {:track-id next-track-id
                                                       :player-pos {:last-stop-point 0}
                                                       :paused? (-> room-state :player :paused?)})))
          [update-client-action]))
      [update-client-action])))
