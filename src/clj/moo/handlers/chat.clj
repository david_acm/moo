(ns moo.handlers.chat
  (:require [moo.handlers.core :as handler]
            [moo.db.core :as db]
            [moo.utils :as utils]))

(defmethod handler/-event-msg-handler :chat/new-message [event-msg]
  (let [content (:content (:?data event-msg))
        user-id (:id (:user event-msg))
        room-id (:room-id event-msg)
        users (:users (db/get-room-by-name room-id))
        user-ids (map #(:id %) users)]
    (when (some #{user-id} user-ids)
      {:room/broadcast [room-id [:chat/new-message {:content content
                                                    :user-id user-id
                                                    :id (utils/uuid)}]]})))
