(ns moo.utils
  (:require [clojure.string :as string]))

(defn assoc-user-name [users item]
  (assoc item :user-name (:name (first (filter #(= (:user-id item) (:id %)) users)))))

(defn get-current-track [db]
  (first (filter #(= (:id %) (:track-id (:player db))) (:tracks db))))

(defn get-room-id []
  "Gets the room-id from the pathname"
  (last (string/split (.-pathname (.-location js/document)) #"/")))

; TODO: could be cool to make this a high order function that accepts
; two maps to return
(defn apply-locally? [{:keys [synced?]} from-server?]
  (or (and (not from-server?) (not synced?))
      (and from-server? synced?)))

(defn get-unix-timestamp [] (int (/ (.now js/Date) 1000)))
