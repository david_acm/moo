(ns moo.chat
  (:require [reagent.core :as r]
            [re-frame.core :as rf]))

(defn chat-box []
  (let [text (r/atom "")]
    (fn []
      [:div
       [:textarea {:type "text"
                   :placeholder "Enter message"
                   :value @text
                   :style {:height "5%" :width "100%" :overflowY "auto"}
                   :on-change #(reset! text (-> % .-target .-value))
                   :on-key-press (fn [event]
                                   (when (and (= 13 (.-charCode event)) ; charCode 13 is the Enter key
                                              (not (empty? @text)))
                                     (.preventDefault event) ; .preventDefault stops :on-change from firing
                                     (rf/dispatch [:chat/send-message {:content @text}])
                                     (reset! text "")))}]])))

(defn is-scrolled-to-bottom? [element]
  (>= (.-scrollTop element) (- (.-scrollHeight element) (.-offsetHeight element))))

(defn chat-messages []
  (let [element (r/atom nil)
        scroll-down? (r/atom false)]
    (r/create-class
     {:display-name "chat-messages"
      :component-will-update #(reset! scroll-down? (is-scrolled-to-bottom? @element))
      :component-did-update #(when @scroll-down? (set! (.-scrollTop @element) (.-scrollHeight @element)))
      :reagent-render
      (fn []
        [:div {:ref #(reset! element %) :style {:height "90%"
                                                :overflowY "auto"
                                                :word-break "break-all"}}
         (map (fn [{:keys [id user-name content]}] [:div {:key id} [:span.chat-name user-name] ": " content])
              @(rf/subscribe [:messages]))])})))

(defn users []
  [:div.users {:style {:height "5%" :border-style "solid" :overflowY "scroll"}}
   (map (fn [{:keys [id name]}] [:div {:key id} name]) @(rf/subscribe [:users]))])

(defn chat [messages]
  [:div
   [users]
   [chat-messages]
   [chat-box]])
