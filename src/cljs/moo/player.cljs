(ns moo.player
  (:require-macros
   [cljs.core.async.macros :as asyncm :refer (go go-loop)])
  (:require [cljs.spec.alpha :as s]
            [cljs.core.async :as async :refer (timeout <! >! put! chan)]
            [reagent.core :as r]
            [re-frame.core :as rf :refer [dispatch reg-fx reg-event-fx reg-event-db reg-sub after]]
            [moo.events :as events :refer [check-spec-interceptor]]
            [moo.utils :as utils]
            [moo.shared.utils :as shared-utils]
            [cljsjs.react-player]))

(defonce player-instance (r/atom nil))

(defonce react-player (r/adapt-react-class js/ReactPlayer))

(defonce sync-loop
  (go-loop []
    (<! (timeout 5000))
    (when-let [pos (:player-pos @(rf/subscribe [:player]))]
      (when @(rf/subscribe [:synced?]) (rf/dispatch [:player/pos-update pos])))
    (recur)))

(defn video-player-component [player-data]
  [react-player {:ref #(rf/dispatch-sync [:player/new-player-instance %])
                 :url (:src (:track player-data))
                 :volume (:volume player-data)
                 :height "100%"
                 :width "100%"
                 :controls false
                 :playing (not (:paused? player-data))
                 :onReady #(rf/dispatch [:player/ready player-data])
                 :onEnded #(rf/dispatch [:player/ended])
                 :onError #(rf/dispatch [:player/ended])
                 :onPlay #(rf/dispatch [:player/playing])}])

(defn start-position-bar-watcher! []
  (go-loop []
    (<! (timeout 100))
    (rf/dispatch [:player/update-position-bar])
    (recur)))

(defn position-bar [{:keys [current-time duration]}]
  (let [value (r/atom 0)
        mouse-down? (r/atom false)]
    (fn [{:keys [current-time duration]}]
      [:div.col-8
       [:input {:type "range"
                :id "position"
                :on-mouse-down #(reset! mouse-down? true)
                :on-mouse-up (fn []
                               (reset! mouse-down? false)
                               (when-let [player @player-instance]
                                 (rf/dispatch [:player/seek {:last-stop-point (* (/ @value 100) (.getDuration player))
                                                             :last-start-timestamp (utils/get-unix-timestamp)}])))
                :value (if @mouse-down? @value (* 100 (/ current-time duration)))
                :on-change #(reset! value (-> % .-target .-value))
                :min 0
                :max 100
                :step 0.0001
                :style {:width "100%"}}]])))

(defn volume-bar [volume]
  (let [value (r/atom 0)
        mouse-down? (r/atom false)]
    (fn [volume]
      [:div.col
       [:input {:type "range"
                :id "position"
                :on-mouse-down #(reset! mouse-down? true)
                :on-mouse-up (fn [] (reset! mouse-down? false) (rf/dispatch [:player/volume @value]))
                :value volume
                :on-change #(reset! value (-> % .-target .-value))
                :min 0
                :max 1
                :step 0.0001
                :style {:width "100%"}}]])))

(defn player-component []
  (let [player-data (assoc @(rf/subscribe [:player]) :volume @(rf/subscribe [:volume]))]
    [:div
     [:div.row.h-50
      [video-player-component player-data]]
     [:div.row {:style {:height "5%"}}
      (if (:paused? player-data)
        [:button {:on-click #(rf/dispatch [:player/play])} "Play"]
        [:button {:on-click #(rf/dispatch [:player/pause (when-let [p @player-instance] (.getCurrentTime p))])} "Pause"])
      (if (:autoplay? player-data)
        [:button {:on-click #(rf/dispatch [:disable-autoplay])} "Disable Autoplay"]
        [:button {:on-click #(rf/dispatch [:enable-autoplay])} "Enable Autoplay"])
      (if @(rf/subscribe [:synced?])
        [:button {:on-click #(rf/dispatch [:player/desync])} "Desync from room"]
        [:button {:on-click #(rf/dispatch [:player/resync])} "ReSync with room"])]
     [:div.row.position-and-volume-bars  {:style {:height "5%"}}
      "Position:" [position-bar @(rf/subscribe [:player-position])]
      "Volume:" [volume-bar (:volume player-data)]]]))


; events


(reg-event-fx
 :player/ready
 (fn [{:keys [db]} [_ player-data]]
   (if (:synced? db)
     {:set-pos (-> db :room-state :player)}
     {:set-pos player-data})))

(reg-event-fx
 :player/ended
 (fn [{:keys [db]} _]
   (if (:synced? db)
     (when-not (:player-ended? db)
       {:ws-send [:player/ended]
        :db (assoc db :player-ended? true)})
     (let [player (-> db :room-state :player)
           tracks (-> db :room-state :tracks)
           current-track-id (:track-id player)]
       {:set-track (merge
                    player
                    {:track-id (:id (shared-utils/get-next-track tracks current-track-id))
                     :player-pos {:last-stop-point 0
                                  :last-start-timestamp (utils/get-unix-timestamp)}})}))))

(reg-event-fx
 :player/playing
 (fn [{:keys [db]} _]
   (when (and (:synced? db)
              (:paused? (:player (:room-state db))))
     {:pause-youtube nil})))

(reg-event-db
 :player/update-position-bar
 (fn [db _]
   (when-let [player @player-instance]
     (assoc db :player-position {:current-time (.getCurrentTime player) :duration (.getDuration player)}))))

(reg-event-fx
 :player/new-player-instance
 [check-spec-interceptor]
 (fn [db [_ player]]
   {:update-player-instance player}))

(reg-event-fx
 :disable-autoplay
 [check-spec-interceptor]
 (fn [_ _]
   {:ws-send [:player/disable-autoplay]}))

(reg-event-fx
 :enable-autoplay
 [check-spec-interceptor]
 (fn [_ _]
   {:ws-send [:player/enable-autoplay]}))

(reg-event-db
 :player/disable-autoplay
 (fn [db _]
   (assoc-in db [:room-state :player :autoplay?] false)))

(reg-event-db
 :player/enable-autoplay
 (fn [db event]
   (assoc-in db [:room-state :player :autoplay?] true)))

(reg-event-fx
 :player/set-track
 [check-spec-interceptor]
 (fn [cofx event]
   (events/syncable
    cofx event
    (fn [{:keys [db]} player-data]
      (let [player-data (if (:last-start-timestamp (:player-pos player-data))
                          player-data
                          (assoc-in player-data [:player-pos :last-start-timestamp] (utils/get-unix-timestamp)))
            player (-> db :room-state :player (merge player-data))]
        {:db (assoc (assoc-in db [:room-state :player] player)
                    :player-ended? false)
         :set-pos player}))
    (fn [d] d))))

(reg-event-fx
 :player/seek
 (fn [cofx event]
   (events/syncable
    cofx event
    (fn [{:keys [db]} pos]
      {:db (assoc (update-in db [:room-state :player :player-pos] merge pos) :player-ended? false)
       :set-pos {:paused? (-> db :room-state :player :paused?)
                 :player-pos pos}})
    (fn [a] (:last-stop-point a)))))

(reg-event-fx
 :player/play
 [check-spec-interceptor]
 (fn [cofx event]
   (events/syncable
    cofx event
    (fn [{:keys [db]} _]
      {:db (assoc-in db [:room-state :player :paused?] false)})
    nil)))

(reg-event-fx
 :player/pause
 [check-spec-interceptor]
 (fn [cofx event]
   (events/syncable
    cofx event
    (fn [{:keys [db]} stop-pos]
      {:db (update-in db [:room-state :player] merge {:paused? true :last-stop-point stop-pos})})
    (fn [stop-pos] stop-pos))))

(reg-event-fx
 :player/desync
 [check-spec-interceptor]
 (fn [{:keys [db]} _]
   {:db (assoc db :synced? false)
    :ws-send [:player/desync]}))

(reg-event-fx
 :player/resync
 [check-spec-interceptor]
 (fn [cofx event]
   (events/syncable
    (assoc-in cofx [:db :synced?] true) event
    (fn [{:keys [db]} {player :player track-order :track-order :as e}]
      (when (and player track-order)
        {:db (update-in (update-in db [:room-state :player] merge player)
                        [:room-state :tracks]
                        #(vec (sort (fn [a b] (< (.indexOf track-order (:id a)) (.indexOf track-order (:id b)))) %)))

         :set-pos player}))
    nil)))

(reg-event-fx
 :player/pos-update
 (fn [{:keys [db]} [_ player-pos]]
   (let [db (assoc-in db [:room-state :player :player-pos] player-pos)]
     {:db db
      :set-pos (-> db :room-state :player)})))

(reg-event-db
 :player/volume
 (fn [db [_ volume]]
   (assoc db :volume volume)))

(reg-sub
 :player-position
 (fn [db _]
   (:player-position db)))

(reg-sub
 :volume
 (fn [db _]
   (:volume db)))

(reg-sub
 :synced?
 (fn [db _]
   (:synced? db)))

; effects

(defn get-pos [{:keys [player-pos paused?]}]
  (if paused?
    (:last-stop-point player-pos)
    (Math/abs (+ (:last-stop-point player-pos) (- (utils/get-unix-timestamp) (:last-start-timestamp player-pos))))))

; NOTE: :set-pos should be the only thing handling the
; amount of wiggle-room allowed for syncronization
(reg-fx
 :set-pos
 (fn [player-data]
   (when-let [player @player-instance]
     (when (and player-data (<= 3 (Math/abs (- (get-pos player-data) (.getCurrentTime player)))))
       (.seekTo player (Math/floor (get-pos player-data)))))))

(reg-fx
 :update-player-instance
 (fn [new-player]
   (reset! player-instance new-player)))

(reg-fx
 :pause-youtube
 (fn []
   (when-let [player @player-instance]
     (.pauseVideo (.getInternalPlayer player)))))

(reg-fx
 :set-track
 #(rf/dispatch [:player/set-track %]))
