(ns moo.events
  (:require [moo.db :as db]
            [cljs.spec.alpha :as s]
            [ajax.core :as ajax]
            [re-frame.core :refer [dispatch reg-fx reg-event-fx reg-event-db reg-sub after]]
            [day8.re-frame.http-fx]
            [moo.ws :as ws]
            [moo.utils :as utils]))


(s/check-asserts true)

(defn check-and-throw
  "Throws an exception if `db` doesn't match the Spec `a-spec`."
  [a-spec db]
  (s/assert a-spec db))

(def check-spec-interceptor (after (partial check-and-throw :moo.db/db)))

(defn syncable [cofx [event-id data {:keys [from-server?]}] apply-locally to-server]
  (if (-> cofx :db :synced?)
    (merge (apply-locally cofx data)
           (when-not from-server? {:ws-send [event-id (when to-server (to-server data))]}))
    (if from-server?
      {}
      (apply-locally cofx data))))

;;dispatchers

(reg-event-db
 :initialize-db
 [check-spec-interceptor]
 (fn [_ _]
   db/default-db))

(reg-event-db
 :set-user-id
 [check-spec-interceptor]
 (fn [db [_ id]]
   (assoc db :user-id id)))

(reg-event-db
 :set-active-page
 [check-spec-interceptor]
 (fn [db [_ page]]
   (assoc db :page page)))

(reg-event-fx
 :join-room
 [check-spec-interceptor]
 (fn [{:keys [db]} [_ data]]
   {:ws-send [:room/user-joined data]
    :db (assoc db :page :room)}))

(reg-event-fx
 :disconnect
 [check-spec-interceptor]
 (fn [event [_ data]]
   {:ws-send [:client/disconnect]}))

(reg-event-db
 :user/new
 [check-spec-interceptor]
 (fn [db [_ user-data]]
   (update-in db [:room-state :users] conj user-data)))

(reg-event-db
 :user/leave
 [check-spec-interceptor]
 (fn [db [_ id]]
   (update-in db [:room-state :users]
              (fn [users]
                (filterv #(not= (:id %) id) users)))))

(reg-event-db
 :chat/new-message
 [check-spec-interceptor]
 (fn [db [_ message]]
   (update db :messages conj (utils/assoc-user-name (:users (:room-state db)) message))))

(reg-event-db
 :track/new
 [check-spec-interceptor]
 (fn [db [_ track]]
   (update-in db [:room-state :tracks] conj (utils/assoc-user-name (:users (:room-state db)) track))))

(reg-event-db
 :track/delete
 [check-spec-interceptor]
 (fn [db [_ id]]
   (update-in db [:room-state :tracks] (fn [tracks] (filterv #(not= id (:id %)) tracks)))))

(reg-event-db
 :track/update
 [check-spec-interceptor]
 (fn [db [_ track]]
   (if (first (filter #(= (:id track) (:id %)) (-> db :room-state :tracks)))
     (update-in db [:room-state :tracks] (fn [ts] (mapv #(if (= (:id track) (:id %)) (merge % track) %) ts)))
     (update-in db [:room-state :tracks] conj track))))

(reg-event-fx
 :track/set-index
 [check-spec-interceptor]
 (fn [cofx event]
   (syncable
    cofx event
    (fn [{:keys [db]} {:keys [id index]}]
      (let [tracks (:tracks (:room-state db))
            track (first (filter #(= id (:id %)) tracks))
            track-index (.indexOf tracks track)
            index (if (> index track-index) (+ 1 index) index)
            new-tracks (replace {track nil} tracks)
            new-tracks (concat
                        (conj (vec (first (split-at index new-tracks))) track)
                        (second (split-at index new-tracks)))
            new-tracks (vec (remove nil? new-tracks))]
        {:db (assoc-in db [:room-state :tracks] new-tracks)}))
    (fn [d] d))))

(reg-event-fx
 :track/move-down
 [check-spec-interceptor]
 (fn [{{{tracks :tracks} :room-state} :db} [_ id]]
   (let [new-index (- (.indexOf (map :id tracks) id) 1)]
     (when (>= new-index 0)
       {:dispatch [:track/set-index {:id id :index new-index}]}))))

(reg-event-fx
 :track/move-up
 [check-spec-interceptor]
 (fn [{{{tracks :tracks} :room-state} :db} [_ id]]
   (let [new-index (+ (.indexOf (map :id tracks) id) 1)]
     (when (> (count tracks) new-index)
       {:dispatch [:track/set-index {:id id :index new-index}]}))))

(reg-event-fx
 :chat/send-message
 [check-spec-interceptor]
 (fn [event [_ message]]
   {:ws-send [:chat/new-message message]}))

(reg-event-db
 :room/state-update
 [check-spec-interceptor]
 (fn [db [_ room-state]]
   (let [db (update db :room-state merge room-state)]
     (if (empty? (filter #(= (:user-id db) (:id %)) (:users (:room-state db))))
       db
       (assoc db :page :room)))))

(reg-event-fx
 :track/add
 [check-spec-interceptor]
 (fn [event [_ track]]
   {:ws-send [:track/new track]}))

(reg-event-fx
  :track/add-youtube
  [check-spec-interceptor]
  (fn [_ [_ url]]
    {:ws-send [:track/new-youtube url]}))

(reg-event-fx
 :track/add-external
 [check-spec-interceptor]
 (fn [event [_ url]]
   {:ws-send [:track/new-file-download url]}))

(reg-event-fx
 :track/remove
 [check-spec-interceptor]
 (fn [_ [_ id]]
   {:ws-send [:track/remove id]}))

(reg-event-fx
 :upload/new
 [check-spec-interceptor]
 (fn [{:keys [db]} [_ file-target]]
   (when file-target
     (let [client-upload-id (int (rand 99999999))]
       {:db (update-in db [:uploads] assoc client-upload-id file-target)
        :ws-send [:track/new-file-upload client-upload-id]}))))

(reg-event-fx
 :upload/start
 [check-spec-interceptor]
 (fn [{:keys [db]} [_ {:keys [client-upload-id track-id start-byte server-filename]}]]
   (let [file-target ((:uploads db) client-upload-id)
         file (-> file-target .-files (.item 0))
         filename (or server-filename (.-name file))
         file-blob (.slice file (or start-byte 0))
         file-size-mb (/ (.-size file-blob) 1000000)]
     (if (<= file-size-mb 100000000)
       {:http-xhrio {:method :post
                     :uri (str "/room/" (utils/get-room-id) "/upload?track-id=" track-id)
                     :body (doto (js/FormData.)
                             (.append "upload-file" file-blob filename))
                     :timeout 90000
                     :response-format (ajax/raw-response-format {:keywords? true})}}
       {:error (str "Attempting to upload a file with a size of " file-size-mb
                    " when the limit is " (:upload-file-size-limit db))}))))

;;subscriptions

(reg-sub
 :page
 (fn [db _]
   (:page db)))

(reg-sub
 :users
 (fn [{:keys [room-state]} _]
   (:users room-state)))

(reg-sub
 :user
 (fn [{:keys [room-state]} [_ id]]
   (filter #(= (:id %) id) (:users room-state))))

(reg-sub
 :messages
 (fn [db _]
   (:messages db)))

(reg-sub
 :tracks
 (fn [{:keys [room-state]} _]
   (:tracks room-state)))

(reg-sub
 :player
 (fn [{:keys [room-state]} _]
   (assoc (:player room-state) :track (utils/get-current-track room-state))))

;;effects


(reg-fx
 :ws-send
 (fn [event]
   (ws/send! event)))

(reg-fx :dispatch #(dispatch %))
